#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, re
import paramiko
from os.path import dirname, realpath, join
from time import time
from pyrrd.rrd import RRA, RRD, DataSource
from pyrrd.graph import Graph, DataDefinition, VariableDefinition, Line, GraphPrint, Area

HOST='192.168.0.1'
PORT=22
USERNAME='monitoring'
CMD='/execute script=bandwidth_test; :delay 2; :put [:pick [/interface monitor-traffic ether1-gateway as-value] 6]; /system script job remove [find script="bandwidth_test"]'
PATH=realpath(dirname(__file__))
OUTPATH='/www/monitoring'
RRDFILE=join(PATH, 'rrdfile.rrd')


def _run_script():
	#return "rx-bits-per-second=25621672\n";
	ssh_client = paramiko.SSHClient()
	ssh_client.load_system_host_keys()
	ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh_client.connect(HOST, port=PORT, username=USERNAME)
	stdin, stdout, stderr = ssh_client.exec_command(CMD)
	output = stdout.readline()
	ssh_client.close()

	return output


def _store_value(value):
	if not os.path.exists(RRDFILE):
		ds1 = DataSource(dsName='rxbps', dsType='GAUGE', heartbeat=330)
		rra1 = RRA(cf='MAX', xff=0.5, steps=1, rows=24*60/5) #day - 5m
		rra2 = RRA(cf='MAX', xff=0.5, steps=2, rows=7*24*60/5/2) #week - 10m
		rra3 = RRA(cf='MAX', xff=0.5, steps=6, rows=31*24*60/5/6) #month - 30m
		rra4 = RRA(cf='MAX', xff=0.5, steps=12, rows=3653*24) #10(max 3 leap yrs) years - 1h
		rras = [rra1, rra2, rra3, rra4]

		rrd = RRD(RRDFILE, ds=[ds1], rra=rras, step=5*60)
		rrd.create()
	else:
		rrd = RRD(RRDFILE)

	rrd.bufferValue(time(), value)
	rrd.update()

def _create_images():
	def1 = DataDefinition(rrdfile=RRDFILE, dsName='rxbps', vname='vrxbps', cdef='MAX')
	vdef1 = VariableDefinition(vname='last_%s' % def1.vname, rpn='%s,LAST' % def1.vname)
	defs = [def1, vdef1]

	area1 = Area(defObj=def1, color='#990000', legend='RX', width=2)
	lbl1 = GraphPrint(vdef1, r'%6.2lf\n')
	lines = [area1, lbl1]

	graphs = [
		dict(filename='ether1-1d.png', start=int(time()-60*60*24), title='Ether1', vertical_label=r'bits\ per\ second'),
		dict(filename='ether1-1w.png', start=int(time()-60*60*24), title='Ether1', vertical_label=r'bits\ per\ second'),
		dict(filename='ether1-1m.png', start=int(time()-60*60*24), title='Ether1', vertical_label=r'bits\ per\ second'),
		dict(filename='ether1-1y.png', start=int(time()-60*60*24), title='Ether1', vertical_label=r'bits\ per\ second'),
		dict(filename='ether1-10y.png', start=int(time()-60*60*24), title='Ether1', vertical_label=r'bits\ per\ second')
    ]

	for p in graphs:
		#graphfile = os.path.join(MEDIA_ROOT, 'rrd-img/grappy-1d.png')
		g = Graph(join(OUTPATH, p['filename']), width=300*2, height=80*2, lower_limit=0, start=p['start'], end=int(time()), title=p['title'], vertical_label=p['vertical_label'])
		g.data.extend(defs)
		g.data.extend(lines)
		g.write()



def main():
	#run monitor
	res = _run_script()
	match = re.search(r'rx-bits-per-second=(\d+)', res)
	if match:
		value = match.groups()[0]

		#add value to RRD db
		_store_value(value)
		_create_images()



if __name__ == '__main__':
	main()
