# Mikrotik bandwidth graph utility

## Overview

This little piece of software attempts to remotely measure the real WAN bandwidth of your ISP on a Mikrotik RouterOS system.

Briefly, it starts downloading a pretty big file from the internet and measures interface RX speed. Then it produces a bunch of graphs like this:

![Sample graph](http://bitbucket.org/ryba_xek/mikrotik-bandwidth-graph/raw/c961f415ad22d6a13932b751253939830b7a0976/demo.png)

## Requirements:

* a Mikrotik RouterOS with ssh service enabled and WAN cable attached
* a Linux box with Python, pyrrd, paramiko and rrdtools installed
* a network connection between those two, ssh access with key pair
* a URL to a near server in the Internet with a pretty big file accessible over http. I would recommend at least 2-4 megabytes.


The measurment is done with an artificial load applied, so the numbers will greatly depend on remote server throughoutput.

The RouterOS box should have this script named ‘*bandwidth_test*’:

    /tool fetch keep-result=no url="{URL}"
where *{URL}* is replaced with a http link to a file on the internet.

You can create script in console with a command like this:

    /system script add name=bandwidth_test policy=read,test,sniff source="/tool fetch keep-result=no url=\"http://speedtest-1.synt.rutube.ru/speedtest/random3000x3000.jpg\""
